<html>
<head>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<title></title>
</head>
<body>

<script>
	
$(document).ready(function(){



	$("#signinbtn").click(function() {
            var username = $("#username").val();
            var password = $("#password").val();
            if ($("#username").val() == "") {
                $(".alert").show();
                return false;
            }

 		$.ajax({  
                url:"logandreg.php",  
                method:"POST",  
                data:{username:username,password:password},  
                dataType:"text", 
                success:function(data)  
                { 
					if(data=="valid")
					{
						$(".alert").show();
						document.getElementById("alert").innerHTML = "Redirecting...";

						 setTimeout(function(){
              			 window.location = "index.php";
						 }, 3000);

						return true;
					}
					else
					{
						$(".alert").show();
 				 		document.getElementById("alert").innerHTML = "Invalid Username Or Password";
 				 		return false;
					}
                }  
           });

	});


		$("#signupbtn").click(function(){
	 var username = $("#username").val();
	var email = $("#email").val();
  	var password = $("#password").val();
	if ($("#username").val()=="") {
  		$(".alert").show();
  		return false;
  	}


 $.ajax({  
                url:"logandreg.php",  
                method:"POST",  
                data:{username:username,password:password,email:email},  
                dataType:"text", 
                success:function(data)  
                { 
					if(data=="valid")
					{
						$(".alert").show();
						document.getElementById("alert").innerHTML = "Registerd Successfully";

						 setTimeout(function(){
              			 window.location = "index.php";
						 }, 3000);


				
					}
					else
					{
 				 		document.getElementById("alert").innerHTML = "Invalid Username Or Password";
 				 		return false;
					}
                }  
           });

	});

		


	$("#emaildiv").hide();
	$("#forsignindiv").hide();
	$("#forgetbtn").hide();
	$("#signupbtn").hide();
	$(".alert").hide();



  $("#forforget").click(function(){
  	$("#signupbtn").hide();
  	$("#signinbtn").hide();
  	$("#userdiv").hide();
  	$("#passdiv").hide();
  	$("#forsignupdiv").hide();
  	$("#forforget").hide();
  	$("#forsignindiv").show();
	$("#emaildiv").show();
  	$("#forgetbtn").show();

  });



  $("#forsignup").click(function(){

  	$("#signbtn").hide();
  	$("#signinbtn").hide();
  	$("#forgetbtn").hide();
	$("#signupbtn").show();
	$("#forforget").hide();
	$("#forsignupdiv").hide();
	$("#forsignindiv").show();
  	$("#passdiv").show();
  	$("#userdiv").show();
    $("#emaildiv").show();
  });


   $("#forsignin").click(function(){


  	$("#signinbtn").show();
  	$("#forgetbtn").hide();
	$("#signupbtn").hide();
  	$("#emaildiv").hide();
   	$("#forsignindiv").hide();
  	$("#forsignupdiv").show();

  	$("#forforget").show();
  	$("#passdiv").show();
  	$("#userdiv").show();
  });
});



</script>


					<?php
						if(isset($_SESSION['username']))
						{

							?>


<div class="container">

<div id="myModal" class="modal fade">
	<div class="modal-dialog modal-login">
		<div class="modal-content">
			<div class="modal-header">				
				<h4 class="modal-title">Sign In</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<form action="#" onsubmit="return false" method="post">
					<div class="form-group" id="userdiv">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-user"></i></span>
							<input type="text" class="form-control" id="username" name="username" placeholder="Username">
						</div>
					</div>
					<div class="form-group" id="passdiv">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-lock"></i></span>
							<input type="text" class="form-control" id="password" name="password" placeholder="Password">
						</div>
					</div>
					<div class="form-group" id="emaildiv">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
							<input type="text" class="form-control" id="email" name="email" placeholder="Email">
						</div>
					</div>
					<div style="color:red;font-style: 30px" class="alert"><h5 id="alert"> Please Fill First
</h5>
</div>
					<div class="form-group">
						<button type="submit" style="border-radius: 2px;background-color:#047bd5;" id="signinbtn" class="btn btn-primary btn-md">Sign In</button>
					</div>
					<div class="form-group">
						<button type="submit" style="border-radius: 2px;background-color:#047bd5;" id="forgetbtn" class="btn btn-primary btn-md">Send Mail</button>
					</div>
						<div class="form-group">
						<button type="submit" style="border-radius: 2px;background-color:#047bd5;" id="signupbtn" class="btn btn-primary btn-md">Sign Up</button>
					</div>
					<p class="hint-text"><a id="forforget" href="#">Forgot Password?</a></p>
				</form>
			</div>
			<div id="forsignupdiv" class="modal-footer">Don't have an account? <a id="forsignup" href="#">Create one</a>
			</div>
			<div id="forsignindiv" class="modal-footer">You have an account? <a id="forsignin" href="#">Sign in</a>
			</div>

		</div>
	</div>
</div>
</div>

	<div class="header">
		<div class="top-header">
			<div class="wrap">
				<div class="header-left">
					<ul>
						<li><a href=""><i class="fa fa-facebook-square" style="font-size:28px;color:#3b5998"></i></a></li>&nbsp;
						<li><a href=""><i class="fa fa-instagram" style="font-size:28px;color:#3f729b"></i></a></li>&nbsp;
						<li><a href=""><i class="fa fa-google-plus-square" style="font-size:28px;color:#dd4b39"></i></a></li>&nbsp;
						<li><a href=""><i class="fa fa-linkedin-square" style="font-size:28px;color:#007bb6"></i></a></li>&nbsp;
						<li><a href=""><i class="fa fa-youtube-play" style="font-size:28px;color:#bb0000"></i></a></li>&nbsp;
						<li><a href=""><i class="fa fa-twitter-square" style="font-size:28px;color:#00aced"></i></a></li>&nbsp;
						<li><a href=""><i class="fa fa-pinterest-square" style="font-size:28px;color:#cb2027"></i></a></li>
					</ul>
				</div>
				<div class="header-right">
					<ul><center>
						<li>
							<a href="account.html">Career</a> | &nbsp;
						</li>
						<li class="login">
							<a href="login.html">Support</a>
						</li>
					</center>
						
						
					</ul>

				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div  class="headerwrap">
			<div class="header-bottomwrap" >

				<div class="row">
					<div class="col-sm-2 logodiv">
						<div class="logo">
						<a href="index.html"><img  style="margin-top:5px;height:60px;border:1px solid white;border-radius: 3px;"src="images/logo.png" class="img-responsive" alt="" /></a>
						</div>
					</div>

					<div class="col-sm-2 cartdiv1">
						<a href=""><i class="glyphicon glyphicon-shopping-cart cartanduser"><span style="background-color: red" class="badge ">5</span></i>&nbsp;&nbsp;</a>
						<a href=""><i class="glyphicon glyphicon-bell cartanduser"><span style="background-color: red" class="badge">6</span></i>&nbsp;&nbsp;</a>	
						<a href="logout.php" ><i class="glyphicon glyphicon-log-out cartanduser"></i>&nbsp;&nbsp;</a>
					</div>
					<div class="col-sm-8">
						<div class="search">
							<div class="search2">
					 		 <form>
								<input type="submit" value="">
						 			<input type="text" value="Search for a product, category or brand" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search for a product, category or brand';}"/>
					  			</form>
							</div>
						</div>	
					</div>
					<div class="col-sm-2 cartdiv2">
						<a href=""><i class="glyphicon glyphicon-shopping-cart cartanduser"><span style="background-color: red"  class="badge">5</span></i>&nbsp;&nbsp;</a>
						<a href=""><i class="glyphicon glyphicon-bell cartanduser"><span style="background-color: red"  class="badge">5</span></i>&nbsp;&nbsp;</a>	
						<a href="logout.php" ><i class="glyphicon glyphicon-log-out cartanduser"></i>&nbsp;&nbsp;</a>


					</div>

				</div>


			
				


				<div class="clearfix"></div>
			</div>
		</div>
	</div>


							<?php

						}

						else
						{
							?>

<div class="container">

<div id="myModal" class="modal fade">
	<div class="modal-dialog modal-login">
		<div class="modal-content">
			<div class="modal-header">				
				<h4 class="modal-title">Sign In</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<form action="#" onsubmit="return false" method="post">
					<div class="form-group" id="userdiv">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-user"></i></span>
							<input type="text" class="form-control" id="username" name="username" placeholder="Username">
						</div>
					</div>
					<div class="form-group" id="passdiv">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-lock"></i></span>
							<input type="text" class="form-control" id="password" name="password" placeholder="Password">
						</div>
					</div>
					<div class="form-group" id="emaildiv">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
							<input type="text" class="form-control" id="email" name="email" placeholder="Email">
						</div>
					</div>
					<div style="color:red;font-style: 30px" class="alert"><h5 id="alert"> Please Fill First
</h5>
</div>
					<div class="form-group">
						<button type="submit" style="border-radius: 2px;background-color:#047bd5;" id="signinbtn" class="btn btn-primary btn-md">Sign In</button>
					</div>
					<div class="form-group">
						<button type="submit" style="border-radius: 2px;background-color:#047bd5;" id="forgetbtn" class="btn btn-primary btn-md">Send Mail</button>
					</div>
						<div class="form-group">
						<button type="submit" style="border-radius: 2px;background-color:#047bd5;" id="signupbtn" class="btn btn-primary btn-md">Sign Up</button>
					</div>
					<p class="hint-text"><a id="forforget" href="#">Forgot Password?</a></p>
				</form>
			</div>
			<div id="forsignupdiv" class="modal-footer">Don't have an account? <a id="forsignup" href="#">Create one</a>
			</div>
			<div id="forsignindiv" class="modal-footer">You have an account? <a id="forsignin" href="#">Sign in</a>
			</div>

		</div>
	</div>
</div>
</div>

	<div class="header">
		<div class="top-header">
			<div class="wrap">
				<div class="header-left">
					<ul>
						<li><a href=""><i class="fa fa-facebook-square" style="font-size:28px;color:#3b5998"></i></a></li>&nbsp;
						<li><a href=""><i class="fa fa-instagram" style="font-size:28px;color:#3f729b"></i></a></li>&nbsp;
						<li><a href=""><i class="fa fa-google-plus-square" style="font-size:28px;color:#dd4b39"></i></a></li>&nbsp;
						<li><a href=""><i class="fa fa-linkedin-square" style="font-size:28px;color:#007bb6"></i></a></li>&nbsp;
						<li><a href=""><i class="fa fa-youtube-play" style="font-size:28px;color:#bb0000"></i></a></li>&nbsp;
						<li><a href=""><i class="fa fa-twitter-square" style="font-size:28px;color:#00aced"></i></a></li>&nbsp;
						<li><a href=""><i class="fa fa-pinterest-square" style="font-size:28px;color:#cb2027"></i></a></li>
					</ul>
				</div>
				<div class="header-right">
					<ul><center>
						<li>
							<a href="account.html">Career</a> | &nbsp;
						</li>
						<li class="login">
							<a href="login.html">Support</a>
						</li>
					</center>
						
						
					</ul>

				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div  class="headerwrap">
			<div class="header-bottomwrap" >

				<div class="row">
					<div class="col-sm-2 logodiv">
						<div class="logo">
						<a href="index.html"><img  style="margin-top:5px;height:60px;border:1px solid white;border-radius: 3px;"src="images/logo.png" class="img-responsive" alt="" /></a>
						</div>
					</div>

					<div class="col-sm-2 cartdiv1">
						<a href=""><i class="glyphicon glyphicon-shopping-cart cartanduser"><span style="background-color: red" class="badge ">0</span></i>&nbsp;&nbsp;</a>
						<a href=""><i class="glyphicon glyphicon-bell cartanduser"><span style="background-color: red" class="badge">0</span></i>&nbsp;&nbsp;</a>	
						<a href="" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-user cartanduser"></i>&nbsp;&nbsp;</a>
					</div>
					<div class="col-sm-8">
						<div class="search">
							<div class="search2">
					 		 <form>
								<input type="submit" value="">
						 			<input type="text" value="Search for a product, category or brand" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search for a product, category or brand';}"/>
					  			</form>
							</div>
						</div>	
					</div>
					<div class="col-sm-2 cartdiv2">
						<a href=""><i class="glyphicon glyphicon-shopping-cart cartanduser"><span style="background-color: red"  class="badge">0</span></i>&nbsp;&nbsp;</a>
						<a href=""><i class="glyphicon glyphicon-bell cartanduser"><span style="background-color: red"  class="badge">0</span></i>&nbsp;&nbsp;</a>	
						<a href="" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-user cartanduser"></i>&nbsp;&nbsp;</a>


					</div>

				</div>


			
				


				<div class="clearfix"></div>
			</div>
		</div>
	</div>

							<?php

						}

					?>




</body>
</html>