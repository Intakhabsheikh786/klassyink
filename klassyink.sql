-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 14, 2019 at 11:46 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.2.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `klassyink`
--

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `src` varchar(35) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `Name`, `src`, `price`) VALUES
(1, '36 Ayegi', 'images/products/7.png', 299),
(2, 'Certified Fit', 'images/products/8.jpg', 300),
(3, 'Alcoholic', 'images/products/9.jpg', 299),
(4, 'Apna Drop Ayega', 'images/products/10.jpg', 300),
(5, 'Dark Night', 'images/products/11.jpg', 299),
(6, 'Love Beer', 'images/products/112.jpg', 300),
(7, 'No 1 Shana', 'images/products/13.jpg', 299),
(17, 'Alcoholic', 'images/products/9.jpg', 299),
(18, 'Apna Drop Ayega', 'images/products/10.jpg', 300),
(19, 'Dark Night', 'images/products/11.jpg', 299),
(20, 'Love Beer', 'images/products/112.jpg', 300),
(21, 'No 1 Shana', 'images/products/13.jpg', 299);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `uid` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `username`, `password`, `email`) VALUES
(1, 'admin', '1234', 'admin@gmail.com'),
(44, 'frre', 'erfr', 'rfe'),
(46, 'rf', 'rf', 'rf'),
(48, 'ed', 'ed', 'ed'),
(50, 'dc', 'rf', 'rffreftr'),
(51, 'fw', 'ff', 'frfr'),
(52, 'r24', 'rf', 'r3'),
(53, 'ef', 'rre', 'fr'),
(54, 'dcd', 'fee', 'ree'),
(75, 'fre', 'fr', 'r'),
(104, 'ds', 'sd', 'ds'),
(108, 'ce', 'rf', 'er'),
(127, 'dg', 'fs', 'sf'),
(128, 'aef', 'ef', 'effe'),
(129, 'sf', 'sfr', 'rsrr');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
