
	<div class="footer">
		<div class="footwrap">
			<div class="contact-section">
				<div class="col-md-4 follow text-left">
					<h3>Follow Us</h3>
					<p>Get THe Latest Update On</p>
					<div class="social-icons">
						<i class="twitter"></i>
						<i class="facebook"></i>
						<i class="googlepluse"></i>
						<i class="pinterest"></i>
						<i class="linkedin"></i>
					</div>
				</div>
				<div class="col-md-4 subscribe text-left">
					<h3>Subscribe Us</h3>
					<p>Get the latest updates & Offers right in your inbox.</p>
					<input type="text" class="text" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '';}">
					<input type="submit" value="Subscribe">
				</div>
				<div class="col-md-4 help text-right">
					<h3>Need Help?</h3>
					<p>For Any Query Visist</p>
					<a href="contact.html">Contact us</a>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="footer-middle">




				<div class="col-md-3 different-products">
					<ul>
						<li class="first"> Shop </li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> Company Profile </a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> Meet The Team </a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> Career </a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> Testimonials </a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> Campus Ambassador </a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> Sell your Artwork </a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> Win A gifts Cards </a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> Media/Press </a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> Investor </a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> F.A.Q </a></li><hr class="foothr">


					</ul>
				</div>
				<div class="col-md-3 different-products">
						<ul>
						<li class="first"> Need Help ?</li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> Graphics T Shirts </a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> Plain T Shirts </a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> Mobiles Covers </a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> Schools And Colleges</a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> Custom Design Order </a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> Design Studio </a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> NGO and Government Organisation</a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href="">Gift Vouchers</a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> KlassyInk Fan</a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> Sitemap</a></li><hr class="foothr">

					</ul>
				</div>
				<div class="col-md-3 different-products">
						<ul>
						<li class="first"> Customer Support </li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> Size Chart </a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> Feedback </a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> Coupon Partners </a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> Track Order </a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> Media Contact </a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> KlassyInk Stores </a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> How It Works </a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> Terms & Conditions </a></li><hr class="foothr">
						<li><i class="glyphicon glyphicon-chevron-right"></i><a href=""> Privacy & Policy </a></li><hr class="foothr">



					</ul>
				</div>
				<div class="col-md-3 different-products">
					<img style="height: 100px;" src="images/about.png">
					<p style="	font-size:20px;"> “We are at our best when we make our products keeping ourselves in mind – we can guarantee our products are great because we, too, would wear them.”</p>
				</div>
			
				<div class="clearfix"></div>
			</div>
			<div class="cards text-center">
				<img src="images/cards.jpg" alt="" />
			</div>
		</div></div>
		<center><h4>&copy; 2019 Klassyink<p></h4>

